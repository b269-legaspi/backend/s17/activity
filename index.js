const promptEnterInfo = function () {
  let fullName = prompt("Enter your full name: ");
  let age = prompt("Enter your age: ");
  let location = prompt("Enter your location: ");

  alert("Thank you for your input");

  console.log("Hello, " + fullName);
  console.log("You are " + age + " years old.");
  console.log("You live in " + location);
};

promptEnterInfo();

const printTopFiveBands = function () {
  const firstBand = "Parokya ni Edgar";
  const secondBand = "Eraserheads";
  const thirdBand = "Silent Sanctuary";
  const fourthBand = "Spongecola";
  const fifthBand = "Kamikazee";

  console.log("1. " + firstBand);
  console.log("2. " + secondBand);
  console.log("3. " + thirdBand);
  console.log("4. " + fourthBand);
  console.log("5. " + fifthBand);
};

printTopFiveBands();

const printTopFiveMovies = function () {
  const firstMovie = "The Godfather";
  const secondMovie = "The Godfather: Part II";
  const thirdMovie = "John Wick";
  const fourthMovie = "John Wick: Chapter 2";
  const fifthMovie = "John Wick: Chapter 3 - Parabellum";

  console.log("1. " + firstMovie);
  console.log("Rotten Tomatoes Rating: 97%");

  console.log("2. " + secondMovie);
  console.log("Rotten Tomatoes Rating: 96%");

  console.log("3. " + thirdMovie);
  console.log("Rotten Tomatoes Rating: 86%");

  console.log("4. " + fourthMovie);
  console.log("Rotten Tomatoes Rating: 89%");

  console.log("5. " + fifthMovie);
  console.log("Rotten Tomatoes Rating: 89%");
};

printTopFiveMovies();

let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();

// console.log(friend1);
// console.log(friend2);
